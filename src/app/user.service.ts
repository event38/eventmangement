import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
 
  isUserLogged: boolean;
  loginStatus: Subject<any>;


  constructor(private http: HttpClient) { 

    this.isUserLogged = false;
    this.loginStatus = new Subject();

  }

  getUser( email:any ,password:any ): any {
    return this.http.get('http://localhost:8080/login/' + email + "/" + password).toPromise() ;
  }


  getLoginStatus(): any {
    return this.loginStatus.asObservable();
  }

  setUserLoggedIn() {
    this.isUserLogged = true;
    this.loginStatus.next(true);
  }
  
  getUserLoggedStatus(): boolean {
    return this.isUserLogged;
  }
  
  setUserLogout() {
    this.isUserLogged = false;

  }

  getAllCountries() {
    return this.http.get('https://restcountries.com/v3.1/all');
  }

  registerUser(user:any) {
   return this.http.post('http://localhost:8080/registerUser/', user);
  }

}