import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  form!: HTMLFormElement;
  emailField!: HTMLElement;
  emailInput!: HTMLInputElement;
  passField!: HTMLElement;
  passInput!: HTMLInputElement;
  cPassField!: HTMLElement;
  cPassInput!: HTMLInputElement;
  phoneField!: HTMLElement;
  phoneInput!: HTMLInputElement;
 

  ngOnInit(): void {
    this.form = document.querySelector("form")!;
    this.emailField = this.form.querySelector(".email-field")!;
    this.emailInput = this.emailField.querySelector(".email")! as HTMLInputElement;
    this.passField = this.form.querySelector(".create-password")!;
    this.passInput = this.passField.querySelector(".password")! as HTMLInputElement;
    this.cPassField = this.form.querySelector(".confirm-password")!;
    this.cPassInput = this.cPassField.querySelector(".cPassword")! as HTMLInputElement;
    this.phoneField = this.form.querySelector(".phone-field")!;
    this.phoneInput = this.phoneField.querySelector(".phone")! as HTMLInputElement;
    this.phoneInput.addEventListener("keyup", this.checkPhone.bind(this));


    this.form.addEventListener("submit", (e) => {
      e.preventDefault();
      this.checkEmail();
      this.createPass();
      this.confirmPass();
      this.emailInput.addEventListener("keyup", this.checkEmail.bind(this));
      this.passInput.addEventListener("keyup", this.createPass.bind(this));
      this.cPassInput.addEventListener("keyup", this.confirmPass.bind(this));
      if (
        !this.emailField.classList.contains("invalid") &&
        !this.passField.classList.contains("invalid") &&
        !this.cPassField.classList.contains("invalid")
      ) {
        location.href = this.form.getAttribute("action")!;
      }
    });

    const eyeIcons = document.querySelectorAll(".show-hide");
    eyeIcons.forEach((eyeIcon) => {
      eyeIcon.addEventListener("click", () => {
        const pInput = eyeIcon.parentElement!.querySelector("input")! as HTMLInputElement;
        if (pInput.type === "password") {
          eyeIcon.classList.replace("bx-hide", "bx-show");
          pInput.type = "text";
        } else {
          eyeIcon.classList.replace("bx-show", "bx-hide");
          pInput.type = "password";
        }
      });
    });
  }

  // Email Validation
  checkEmail(): void {
    const emailPattern = /^[^ ]+@[^ ]+\.[a-z]{2,3}$/;
    if (!this.emailInput.value.match(emailPattern)) {
      return this.emailField.classList.add("invalid");
    }
    this.emailField.classList.remove("invalid");
  }

  // Password Validation
  createPass(): void {
    const passPattern =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
    if (!this.passInput.value.match(passPattern)) {
      return this.passField.classList.add("invalid");
    }
    this.passField.classList.remove("invalid");
  }

  // Confirm Password Validation
  confirmPass(): void {
    if (this.passInput.value !== this.cPassInput.value || this.cPassInput.value === "") {
      return this.cPassField.classList.add("invalid");
    }
    this.cPassField.classList.remove("invalid");
  }
// check phone number
  checkPhone(): void {
    const phonePattern = /^\d{10}$/; // regex pattern to match 10 digits
    if (!this.phoneInput.value.match(phonePattern)) {
      return this.phoneField.classList.add("invalid");
    }
    this.phoneField.classList.remove("invalid");
  }
  

}
