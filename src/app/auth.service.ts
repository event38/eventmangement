import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject, of } from 'rxjs';

export const AUTH_SERVICE = 'AUTH_SERVICE';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private user : any;
  private isUserLogged: boolean;
  private loginStatus: Subject<any>;

  constructor(private http: HttpClient) {
    this.isUserLogged = false;
    this.loginStatus = new Subject();
  }

  getUser(loginForm : any): any {
    return this.http.get('http://localhost:8080/login/' + loginForm.email + "/" + loginForm.password);
  }

  logout(): void {
    localStorage.removeItem('currentUser');
  }

  isLoggedIn(): boolean {
    return localStorage.getItem('currentUser') !== null;
  }

  getCurrentUser(): any {
    const user = localStorage.getItem('currentUser');
    return user ? JSON.parse(user) : null;
  }

  register(username: string, email: string, password: string): Observable<any> {
    const newUser = { username, email, password };
    this.user.push(newUser);
    return of(newUser);
  }
}