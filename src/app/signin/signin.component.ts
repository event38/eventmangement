import { Component, Output, EventEmitter } from '@angular/core';
import { UserService } from '../user.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent {
  user: any;
  email: any;
  password: any;
  signInForm: FormGroup ;

  @Output() signInSuccess: EventEmitter<any> = new EventEmitter<any>();

  constructor(private service: UserService) { 
    this.signInForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(6)])
    });
  }

  async signIn(signInForm: any) {
    console.log(signInForm);
    console.log(signInForm.email, signInForm.password);
   
    await this.service.getUser(this.email, this.password).then((data: any) => {
      this.user = data;
      this.signInSuccess.emit(this.user);

    })
    if(this.user!=null){

    }else{
      
    }
  }

}
